angular.module('starter.services', ['ngSanitize', 'ui.select'])

/**
 * A simple example service that returns some data.
 */
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      out = items;
    }

    return out;
  };
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show')
        return config
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide')
        return response
      }
    }
  })
})
.run(function($rootScope, $ionicLoading,$window,$location) {
  $rootScope.$on('loading:show', function() {
    $ionicLoading.show({template: '<span style="font-size:32px"><i class="icon ion-looping"></i></span>'})
  })

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide()
  })
  //Set the default timeout for http calls
  $rootScope.timeout=7000;
  
  $rootScope.apiUrlHomeGet="http://lion.27coupons.com/v1.0/get/";
  if ($window.localStorage["userInfo"]!="{}" && $window.localStorage["userInfo"]!="undefined" &&$window.localStorage["userInfo"]!=null ) {
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $rootScope.apiKeyOfUser=userInfo.apiKey;
    if($location.url()=="/app/splash")
    {
      $location.path("app/home");
    }
    }else{
      $location.path("/app/splash");
    }

})

.service("home_page", ["$http","$rootScope","$window","$ionicModal","$ionicNavBarDelegate", function ($http,$rootScope,$window,$ionicModal,$ionicNavBarDelegate) {
      
      if($rootScope.apiKeyOfUser==null){
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            $rootScope.apiKeyOfUser=userInfo.apiKey;
          
        }
    this.gettabs = function ($scope,$ionicLoading) {

          var Apikey = $rootScope.apiKeyOfUser;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/coupons/home-page/',
                data: $.param({ 
                        key:Apikey,
                        //id:cpnId
                    }),
                timeout : $rootScope.timeout, 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(result){
                    $scope.tabs=result.response;
                    if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                    if(result.response.push_notifications.new != 0 )
                    {
                      $scope.not_hide = 1;
                    }
                }).error(function(data, status, header, config){
                  //alert(config.timeout);
                  $scope.message="Please try again later";

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    };
                });

    }
}])
.service("single_coupon", ["$http","$rootScope", function ($http,$rootScope) {
    this.details = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"coupons/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}})
                .success(function (result) {
                    $scope.single_coupon=result.response.coupon;
                });

    }
}])

.service("notification", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.show = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"notifications/list-all/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}})
                .success(function (result) {
                    $scope.notification=result.response;
                     //$scope.tabs=result.response;
                    if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                });

    }
    this.delete = function ($scope){
      $http.get($rootScope.apiUrlHomeGet+"notifications/remove-notifications/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                })
    }
}])
.service("notification_detail", ["$http","$rootScope","$location","$ionicModal", function ($http,$rootScope,$location,$ionicModal) {
    this.select = function ($scope) {
        $http.get("http://lion.27coupons.com/"+$scope.api_node+"?key="+$rootScope.apiKeyOfUser,{params: {id: $scope.id}})
                .success(function (result) {
                    if(($scope.category == "coupons") && ($scope.fourcharacter == "list-single"))
                    {
                       $scope.single_coupon=result.response;
                       $location.path("/app/couponsingle/"+$scope.id);
                    }
                    else if(($scope.category == "notifications") && ($scope.fourcharacter == "list-html"))
                    {
                      
                          $scope.message=result.response.content;

                          $ionicModal.fromTemplateUrl('templates/notification_html.html', {
                            scope: $scope
                          }).then(function(modal) {
                            $scope.modal = modal;
                            $scope.modal.show();
                          });

                          // Triggered in the login modal to close it
                          $scope.closeLogin = function() {
                            $scope.modal.hide();
                            $('.outer-div').removeClass('blur-in');   
                          }; 
                        
                    }
                    else if(($scope.category == "alerts") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.listsingle=result.response;
                       $location.path("/app/alertsingle/"+$scope.id);
                    }
                     else if(($scope.category == "stores") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.single_stores=result.response;
                       $location.path("/app/storelistsingle/"+$scope.id);
                    }
                     else if(($scope.category == "categories") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.categories_listsingle=result.response;
                       $location.path("/app/categoriessingle/"+$scope.id);
                    }
                     else if(($scope.category == "banks") && ($scope.fourcharacter == "list-single"))
                    {
                        $scope.banksingledetails=result.response;
                       $location.path("/app/banksingle/"+$scope.id);
                    }
                });

    }
}])
.service("points", ["$http","$rootScope", function ($http,$rootScope) {
    this.show = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"misc/points-summary/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.points=result.response.data;
                    if(result.response.data.total_points == null)
                    {
                      $scope.points.total_points = 0;
                    }
                });

    }
}])
.service("favourite_stores", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.favstores = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"stores/favorites/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                  $scope.favourite_stores = result.response.data;
                });

    }
}])
.service("single_stores", ["$http","$rootScope", function ($http,$rootScope) {
    this.details = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"stores/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}})
                .success(function (result) {
                    $scope.single_stores=result.response.data;
                });

    }
}])
.service("featured", ["$http","$rootScope", function ($http,$rootScope) {
    this.feature= function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"stores/featured/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.featured=result.response.data;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                });

    }
}])
.service("search", ["$http","$rootScope", function ($http,$rootScope) {
   /* this.store= function ($scope,search_result) {
        $http.get($rootScope.apiUrlHomeGet+"stores/search/?key="+$rootScope.apiKeyOfUser,{params: {name: search_result}})
                .success(function (result) {
                    $scope.search=result.response.data;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                });

    }*/
}])
.service("search_search", ["$http","$rootScope", function ($http,$rootScope) {
    this.store= function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"stores/search/?key="+$rootScope.apiKeyOfUser,{params: {name: $scope.search_text}})
                .success(function (result) {
                    $scope.search=result.response.data;
                });

    }
}])
.service("coupon", ["$http","$rootScope", function ($http,$rootScope) {
    this.saved = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"coupons/list-saved-coupons/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.saved=result.response.data;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                });

    }
}])
.service("categories_listall", ["$http","$rootScope", function ($http,$rootScope) {
    this.categorylist = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"categories/list-all/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.categories_listall=result.response.data;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                });

    }
}])
.service("categories_listsingle", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.categorydetails = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"categories/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}})
                .success(function (result) {
                    $scope.categories_listsingle=result.response.data;
                     if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                  }
                });

    }
}])
.service("banks", ["$http","$rootScope", function ($http,$rootScope) {
    this.banklist = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"banks/list-all/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.bank_list=result.response;
                    if (result.response.push_notifications.new == 0) {
                     $scope.notification=result.response.push_notifications;
                     $scope.noti_hide = true;
                    };
                });
                return 1;
                    
    }
}])
.service("bankdetails", ["$http","$rootScope", function ($http,$rootScope) {
    this.banksingledetails = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"banks/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}})
                .success(function (result) {
                    $scope.banksingledetails=result.response.data;
                });

    }
}])
.service("alert", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.listsingle = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"alerts/list-all/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.listsingle=result.response.data;
                     if(result.status == 304)
                    {
                    $scope.message=result.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                      $('.favorite').removeClass('item')
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();   
                    }; 
                  }
                });

    }
}])
.service("single_alert", ["$http","$rootScope", function ($http,$rootScope) {
    this.listsingle = function ($scope,$id) {
        $http.get($rootScope.apiUrlHomeGet+"alerts/list-single/?key="+$rootScope.apiKeyOfUser,{params: {id: $id}})
                .success(function (result) {
                    $scope.listsingle=result.response.data;

                });

    }
}])
.service("create_alert", ["$http","$rootScope", function ($http,$rootScope) {
    this.create = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"alerts/create-alert/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                    $scope.store = result.response.stores;
                    $scope.categories = result.response.categories;
                });

    }
}])
.service("delete_alert", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
        this.del = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.alert_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/delete-alert/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
}])
.service("activate_alert", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
        this.activate = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.alert_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/activate-alert/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
}])
.service("pause_alert", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
        this.pause = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.alert_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/pause-alert/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
}])
.service("save_alert", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.create = function ($form,$scope) {
        var user_alert_store_ids="[";
        for(i=0;i<$form.store_id.length;i++){
          user_alert_store_ids+=$form.store_id[i]+',';
        }
        user_alert_store_ids= user_alert_store_ids.substring(0, user_alert_store_ids.length - 1);
        user_alert_store_ids+="]";

        var user_alert_cat_ids="[";
        for(i=0;i<$form.categories_id.length;i++){
          user_alert_cat_ids+=$form.categories_id[i]+',';
        }
        user_alert_cat_ids= user_alert_cat_ids.substring(0, user_alert_cat_ids.length - 1);
        user_alert_cat_ids+="]";

        var user_alert_schedule="[";
        for(i=0;i<$form.schedule_id.length;i++){
          user_alert_schedule+=$form.schedule_id[i]+',';
        }
        user_alert_schedule= user_alert_schedule.substring(0, user_alert_schedule.length - 1);
        user_alert_schedule+="]";

        var Apikey = $rootScope.apiKeyOfUser;
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/alerts/save-alert/',
                data: $.param({ 
                        key:Apikey,
                        user_alert_name:$form.username,
                        user_alert_store_ids:user_alert_store_ids,
                        user_alert_cat_ids:user_alert_cat_ids,
                        user_alert_schedule:user_alert_schedule,
                        user_alert_coupons_only:$form.coupon
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){

                     $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
}])
.service("authenticationSvc", function($http, $q, $window,$location,$rootScope,$timeout) {
  var userInfo={};

  this.login=function (username,password,$scope) {
    var deferred = $q.defer();
    $http.get("http://lion.27coupons.com/v1.0/post/users/login/?user_email="+username+"&user_password="+password+"&user_network=27coupons")
    .success(function(result) {
        if(result.status==200)
        {
            userInfo = {
                  apiKey: result.response.user_api_key,
                  userEmail:username     
            };
            $timeout(function() {
            $scope.closeLogin();
              }, 1000);
            $('.nav-bar').show();
            $window.localStorage["userInfo"] = JSON.stringify(userInfo);
            $location.path("/app/home");
            $('.button-clear').remove();
            deferred.resolve(userInfo);

        }else{

            $('.error2').addClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error3').addClass('ng-hide');
            $('.sign-in-error').css('display','block');
            $scope.message = result.response;
        }
    }, function(error) {
      deferred.reject(error);
    });
 
    return deferred.promise;
  }
 

  //Register
  this.register= function (username,password,$scope) {
    var deferred = $q.defer();
    $http.get("http://lion.27coupons.com/v1.0/post/users/register/?user_email="+username+"&user_password="+password+"&user_network=27coupons")
    .success(function(result) {
      if(result.status==200)
      {
        userInfo = {
                    apiKey: result.response.user_api_key,
                    userEmail:username     
                   };
      $window.localStorage["userInfo"] = JSON.stringify(userInfo);
      $scope.message = result.response;
      deferred.resolve(userInfo);
       $('.nav-bar').show();
      $scope.modal.hide();
      $location.path("/app/home");
      }else{
        $('.sign-in-error').css('display','block');
         $scope.message = result.response;
      }
        
    }, function(error) {
      deferred.reject(error);
    });
 
    return deferred.promise;
  }


  //Reset Password
  this.resetPassword=function(userName,$scope){
    $http.get("http://lion.27coupons.com/v1.0/post/users/reset-password/?user_email="+userName)
                .success(function (result) {
                    $scope.message = result.response;
                });

  }
  //Login with facebook
  this.loginWithFacebook=function (username) {
    var deferred = $q.defer();
    $http.get("http://lion.27coupons.com/v1.0/post/users/login/?user_email="+username+"&user_network=facebook")
    .success(function(result) {
        if(result.status==200)
        {
            userInfo = {
                  apiKey: result.response.user_api_key,
                  userEmail:username     
            };
            $window.localStorage["userInfo"] = JSON.stringify(userInfo);
            $location.path("/app/home");
            deferred.resolve(userInfo);
        }else{
        }
    }, function(error) {
      deferred.reject(error);
    });
 
    return deferred.promise;
  }
 
})
.service("favorite", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.save = function ($scope) {
    
          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.coupon_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/coupons/save-coupon/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_coupon.fav = 1;
                    $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });
 

    }
     this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.coupon_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/coupons/remove-coupon/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_coupon.fav = 0;
                    $scope.message=data.response;

                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }
}])
.service("category_save", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.save = function ($scope) {

           var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.category_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/save-category/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                  $scope.categories_listsingle.category.fav = 1;
                  $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $('.error').addClass('ng-hide');
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
     this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.category_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/remove-category/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.categories_listsingle.category.fav = 0;
                    $scope.message=data.response;

                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $('.error1').addClass('ng-hide');
                      $scope.modal = modal;
                      $('.error1').addClass('ng-hide');
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }
}])
.service("category_subscribe", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.subscribe = function ($scope) {
          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.category_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.categories_listsingle.category.alert = 1;
                    $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
      this.unsubscribe = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.category_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/un-subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.categories_listsingle.category.alert = 0;
                    $scope.message=data.response;

                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }

}])
.service("bank_save", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.save = function ($scope) {

            var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.bank_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/save-bank/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.banksingledetails.bank.fav = 1;
                    $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
         this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.bank_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/remove-bank/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.banksingledetails.bank.fav = 0;
                    $scope.message=data.response;

                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }

}])
.service("bank_subscribe", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.subscribe = function ($scope) {

               var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.bank_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.message=data.response;
                    $scope.banksingledetails.bank.alert = 1;
                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
       this.unsubscribe = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/banks/un-subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.message=data.response;
                    $scope.banksingledetails.bank.alert = 0;
                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }
}])
.service("store_save", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.save = function ($scope) {

          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/save-store/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_stores.store.fav = 1;
                    $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }

     this.remove = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/remove-store/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_stores.store.fav = 0;
                    $scope.message=data.response;

                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }
}])
.service("store_subscribe", ["$http","$rootScope","$ionicModal", function ($http,$rootScope,$ionicModal) {
    this.subscribe = function ($scope) {

          var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/stores/subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.single_stores.store.alert = 1;
                    $scope.message=data.response;

                    $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                    // Triggered in the login modal to close it
                    $scope.closeLogin = function() {
                      $scope.modal.hide();
                      $('.outer-div').removeClass('blur-in');   
                    }; 
                });

    }
    this.unsubscribe = function ($scope) {
         var Apikey = $rootScope.apiKeyOfUser;
          var cpnId=$scope.store_id;
              
                $http({
                method: 'POST',
                url: 'http://lion.27coupons.com/v1.0/post/categories/un-subscribe/',
                data: $.param({ 
                        key:Apikey,
                        id:cpnId
                    }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $scope.message=data.response;    
                    if(data.status == 200) {                   
                    $scope.single_stores.store.alert = 0;
                    }            
                     $ionicModal.fromTemplateUrl('templates/save.html', {
                      scope: $scope
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                    });

                      // Triggered in the login modal to close it
                      $scope.closeLogin = function() {
                        $scope.modal.hide();
                        $('.outer-div').removeClass('blur-in');   
                      }; 
                        });

    }
}])
.service("favorite_del", ["$http","$rootScope", function ($http,$rootScope) {
    this.delete = function ($scope,id) {
        $http.get($rootScope.apiUrlHomeGet+"coupons/save-coupon/?key="+$rootScope.apiKeyOfUser,{params: {id: id}})
                .success(function (result) {
                });

    }
}])
.service("refer", ["$http","$rootScope", function ($http,$rootScope) {
    this.earn = function ($scope) {
        $http.get($rootScope.apiUrlHomeGet+"misc/refer-friend/?key="+$rootScope.apiKeyOfUser)
                .success(function (result) {
                      $scope.headerText= result.response.data.top_html;
                      $scope.linkToBeSharedViaEmail= result.response.data.referral_url;
                      $scope.linkToBeShared= result.response.data.referral_url;
                      $scope.messageText= result.response.data.email_body;
                      $scope.messageSubject= result.response.data.email_subject;
                });

    }
}]);
