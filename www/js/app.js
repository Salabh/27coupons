angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova'])

.run(function ($ionicPlatform, $location, $rootScope, $window, $state) {

   // <summary>Checks for an authorized user when the URL changes and redirects to the Login screen if unauthorized</summary>
   // <param name="userInfo">Takes the user email from and checks it with the value present on the Local Storage.</param>
   // <returns></returns>
   // <remarks></remarks>

 $rootScope.$on("$locationChangeStart", function(userInfo) {
    var userInfo={};
    if($location.url()=="/app/register" || $location.url()=="/app/resetPassword" || $location.url()=="/app/splash" || $location.url()=="/app/login")
    {

    }
    else if ($window.localStorage["userInfo"]!="{}" && $window.localStorage["userInfo"]!="undefined" && $window.localStorage["userInfo"]!=null) {
    userInfo = JSON.parse($window.localStorage["userInfo"]);
    }else{
      $location.path("/app/splash");
    }

  });


  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $ionicPlatform.registerBackButtonAction(function (event) {
      // <summary>This function handles the back button operation on Android and iOS</summary>
      // <returns>Returns the user to previous history. If the user is at home or login screen, it exits the app.</returns>
      // <remarks></remarks>
    if($state.current.name=="app.home" || $state.current.name=="app.splash" ){
      //alert("inside home called");
      navigator.app.exitApp();
    }
    else {
      navigator.app.backHistory();
    }
    event.preventDefault();
    return false;
  }, 101);
})


.config(function($stateProvider, $urlRouterProvider,$httpProvider) {
    // <summary>Ionic uses AngularUI Router which uses the concept of states. Set up the various states which the app can be in.</summary>
    // <returns>Each state's controller can be found in controllers.js</returns>
    // <remarks>Learn more here: https://github.com/angular-ui/ui-router</remarks>
  
  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html"
    })   

    // Each tab has its own nav history stack:
   // <param name="url">Defines the routes</param>
   // <param name="abstract">To prepend url to child state urls. Need its own ui-view</param>
   // <param name="templateUrl">Defines the URL of the partial view.</param>
   // <param name="controller">Defines the controller associated with the view.</param>
   // <returns></returns>
   // <remarks></remarks>
    .state('app.splash', {
      url: '/splash',
      views: {
        'menuContent': {
          abstract: true,
          templateUrl: 'templates/splash.html',
          controller: 'SplashCtrl'
        }
      }
    })
    .state('app.test', {
      url: '/test',
      views: {
        'menuContent': {
          abstract: true,
          templateUrl: 'templates/test.html',
          controller: 'TestCtrl'
        }
      }
    })
   
    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })
    .state('app.couponsingle', {
      url: '/couponsingle/:couponId',
      views: {
        'menuContent': {
          templateUrl: 'templates/couponsingle.html',
          controller: 'CouponCtrl'
        }
      }
    })
    .state('app.saved', {
      url: '/saved',
      views: {
        'menuContent': {
          templateUrl: 'templates/saved_coupons.html',
          controller: 'SavedCouponCtrl'
        }
      }
    })
    .state('app.favouritestores', {
      url: '/favouritestores',
      views: {
        'menuContent': {
          templateUrl: 'templates/favouritestores.html',
          controller: 'FavstoresCtrl'
        }
      }
    })
    .state('app.storelistsingle', {
      url: '/storelistsingle/:storeId',
      views: {
        'menuContent': {
          templateUrl: 'templates/singlestores.html',
          controller: 'SinglestoreCtrl'
        }
      }
    })
    .state('app.stores', {
      url: '/stores',
      views: {
        'menuContent': {
          templateUrl: 'templates/stores.html',
          controller: 'StoresCtrl'
        }
      }
    })
    .state('app.categories', {
      url: '/categories',
      views: {
        'menuContent': {
          templateUrl: 'templates/categories.html',
          controller: 'CategorieslistallCtrl'
        }
      }
    })
    .state('app.categoriessingle', {
      url: '/categoriessingle/:categoryId',
      views: {
        'menuContent': {
          templateUrl: 'templates/categoriessingle.html',
          controller: 'CategorieslistsingleCtrl'
        }
      }
    })
    .state('app.banks', {
      url: '/banks',
      views: {
        'menuContent': {
          templateUrl: 'templates/banks.html',
          controller: 'BankCtrl'
        }
      }
    })
     .state('app.banksingle', {
      url: '/banksingle/:bankId',
      views: {
        'menuContent': {
          templateUrl: 'templates/bankdetails.html',
          controller: 'BanksingleCtrl'
        }
      }
    })
        .state('app.alert', {
      url: '/alert',
      views: {
        'menuContent': {
          templateUrl: 'templates/alert.html',
          controller: 'AlertCtrl'
        }
      }
    })
      .state('app.alertsingle', {
      url: '/alertsingle/:alertId',
      views: {
        'menuContent': {
          templateUrl: 'templates/alertsingle.html',
          controller: 'AlertsingleCtrl'
        }
      }
    })
     .state('app.createalert', {
      url: '/createalert',
      views: {
        'menuContent': {
          templateUrl: 'templates/createalert.html',
          controller: 'CreateAlertCtrl'
        }
      }
    })
     .state('app.resetPassword', {
      url: '/resetPassword',
      views: {
        'menuContent': {
          templateUrl: 'templates/resetPassword.html',
          controller: 'LoginCtrl'
        }
      }
    })
     .state('app.refer_earn', {
      url: '/refer_earn',
      views: {
        'menuContent': {
          templateUrl: 'templates/refer_earn.html',
          controller: 'ReferEarnCtrl'
        }
      }
    })
     .state('app.notification', {
      url: '/notification',
      views: {
        'menuContent': {
          templateUrl: 'templates/notification.html',
          controller: 'NotificationCtrl'
        }
      }
    })

     .state('app.points', {
      url: '/points',
      views: {
        'menuContent': {
          templateUrl: 'templates/points.html',
          controller: 'PointsCtrl'
        }
      }
     })
    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            }
        }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');

});

