angular.module('starter.controllers', ['ionic', 'ionic.contrib.ui.cards', 'ngCordova', 'ngSanitize', 'ui.select'])
   // <summary>This controller handles the login view and all the user account related stuff</summary>
   // <param name="$scope">Its a variable that is available to both the controller and view</param>
   // <param name="$ionicModal">Provider that handles the POP up screen.</param>
   // <param name="authenticationSvc">A service that handles the authentication of user. defined in service.js</param>
   // <param name="$cordovaFacebook">Its the Facebook cordova plugin that handles the native FB login</param>
   // <param name="$ionicSideMenuDelegate">A service from ionic that handles the side menu</param>
   // <returns></returns>
   // <remarks></remarks>
.controller('SplashCtrl', function ($scope, $timeout, $ionicModal, authenticationSvc, $cordovaFacebook, $location, $ionicSideMenuDelegate) {
    //Stop the user from viewing the menu on the login screen
    $ionicSideMenuDelegate.canDragContent(false);
    $scope.init = function () {
        $(".splash-login").hide();
        $('.nav-bar').hide();
        $(".splash-login").slideUp("slow", function () {
        });
    }
    $timeout(function () {
        $('.loader-icon').fadeOut();
        $(".splash-login").slideDown("slow", function () {
            // Animation complete.
        });
    }, 1000);
    $scope.login_click = 1;
    $scope.register_click = 0;

    $scope.loginData = {};

    $scope.$on('modal.hidden', function () {
        $('.outer-div').fadeIn();
    });

    // Create the login modal that we will use later
    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
        // $scope.modal.hide();
        $scope.modal.remove();
        $('.outer-div').fadeIn();
    };

    // Open the login modal
    $scope.login = function (val) {
        if (val == 1) {
            $ionicModal.fromTemplateUrl('templates/login.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
                $('.sign-in-error').css('display', 'none');
                $('.outer-div').addClass('cover');
                $('.outer-div').fadeOut();
            });

        }
        else {
            $ionicModal.fromTemplateUrl('templates/register.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
                $('.sign-in-error').css('display', 'none');
                $('.outer-div').addClass('cover');
                $('.outer-div').fadeOut();
            });
        }
    };

    //Login with facebook
    $scope.loginWithFacebook = function () {
        //alert("Hi");
        var permissions = ["public_profile", "email", "user_friends"];

        $cordovaFacebook.login(permissions)
        .then(function (success) {

            $cordovaFacebook.api("me", ["public_profile"])
             .then(function (success) {

                 authenticationSvc.loginWithFacebook(success.email);
                 $('.nav-bar').show();
             }, function (error) {
             });


        }, function (error) {
            // error
        });


    };
    //Reset the password
    $scope.resetPassword = function () {
        authenticationSvc.resetPassword($scope.loginData.username, $scope);
        $('.sign-in-error').css('display', 'block');
    }


    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
       

        $('.custom-error-container').addClass('ng-hide');
        $('.custom-error-container1').addClass('ng-hide');
        $('.error1').addClass('ng-hide');
        $('.error3').addClass('ng-hide');
        $('.error2').addClass('ng-hide');
        //$('.sign-in-error').addClass('ng-hide');

        if (email == "" && password == "") {
            $('.custom-error-container').removeClass('ng-hide');
            $('.custom-error-container1').removeClass('ng-hide');
            $('.error1').removeClass('ng-hide');
            $('.error3').removeClass('ng-hide');
            return false;
        }
        if (email == "") {
            $('.custom-error-container').removeClass('ng-hide');
            $('.error2').addClass('ng-hide');
            $('.error3').addClass('ng-hide');
            $('.error1').removeClass('ng-hide');
            return false;
        }
        else if ((atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) && password == "") {
            $('.custom-error-container').removeClass('ng-hide');
            $('.custom-error-container1').removeClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error3').removeClass('ng-hide');
            $('.error2').removeClass('ng-hide');
            return false;
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            $('.custom-error-container').removeClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error3').addClass('ng-hide');
            $('.error2').removeClass('ng-hide');
            return false;
        }
        if (password == "") {
            $('.custom-error-container1').removeClass('ng-hide');
            $('.error3').removeClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error2').addClass('ng-hide');
        }
        else {
            authenticationSvc.login($scope.loginData.username, $scope.loginData.password, $scope);
        }
    };
    //Registers a new user on the system
    $scope.register = function () {
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
        $('.custom-error-container').addClass('ng-hide');
        $('.custom-error-container1').addClass('ng-hide');
        $('.error1').addClass('ng-hide');
        $('.error3').addClass('ng-hide');
        $('.error2').addClass('ng-hide');
        //$('.sign-in-error').addClass('ng-hide');

        if (email == "" && password == "") {
            $('.custom-error-container').removeClass('ng-hide');
            $('.custom-error-container1').removeClass('ng-hide');
            $('.error1').removeClass('ng-hide');
            $('.error3').removeClass('ng-hide');
            return false;
        }
        if (email == "") {
            $('.custom-error-container').removeClass('ng-hide');
            $('.error2').addClass('ng-hide');
            $('.error3').addClass('ng-hide');
            $('.error1').removeClass('ng-hide');
            return false;
        }
        else if ((atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) && password == "") {
            $('.custom-error-container').removeClass('ng-hide');
            $('.custom-error-container1').removeClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error3').removeClass('ng-hide');
            $('.error2').removeClass('ng-hide');
            return false;
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            $('.custom-error-container').removeClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error3').addClass('ng-hide');
            $('.error2').removeClass('ng-hide');
            return false;
        }
        if (password == "") {
            $('.custom-error-container1').removeClass('ng-hide');
            $('.error3').removeClass('ng-hide');
            $('.error1').addClass('ng-hide');
            $('.error2').addClass('ng-hide');
        }
        else {
            authenticationSvc.register($scope.loginData.username, $scope.loginData.password, $scope);
            $('.outer-div').removeClass('blur-in');
        }
    };
    //Loads the forgot user 
    $scope.forgotpassword = function () {
        $scope.modal.hide();
        $ionicModal.fromTemplateUrl('templates/resetPassword.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
            $('.outer-div').addClass('cover blur-in');
            $scope.modal.show();
            $('.sign-in-error').css('display', 'none');
        });
        $('.outer-div').removeClass('blur-in');
    };

})
 // <summary>Handles the logout, clean the local storage and reset the apiKey</summary>
 
.controller('AppCtrl', function ($scope, $ionicModal, $timeout, $window, $location, $rootScope) {
    $scope.logOut = function () {
        delete $window.localStorage["userInfo"];
        delete $window.localStorage["location"];
        $rootScope.apiKeyOfUser = "";
        $location.path("/app/splash");
    }
})
   // <summary>Gets the data for the home page and  sends the Location information</summary>
   // <param name="$scope">Its a variable that is available to both the controller and view</param>
   // <param name="$ionicModal">Provider that handles the POP up screen.</param>
   // <param name="homePageService">A service that handles the homepage data. defined in service.js</param>
   // <param name="$ionicNavBarDelegate">Handles the back button on the view page</param>
   // <param name="locationService">Sends the geo location to the server</param>
   // <returns></returns>
   // <remarks></remarks>
.controller('HomeCtrl', function ($scope, homePageService, $ionicLoading, $location, $ionicNavBarDelegate, $window, locationService) {
    $ionicNavBarDelegate.showBackButton(false);
    $scope.error = false;
    //Calls the homepage service and receives the data for the Home page
    homePageService.gettabs($scope);

    //checkes if the location is sent to the server or not, if not then send the location
    if ($window.localStorage["location"] == "{}" || $window.localStorage["location"] == "undefined" || $window.localStorage["location"] == null) {
        
        locationService.sendLocation();

    } else {
        var locationInfo = JSON.parse($window.localStorage["location"]);
        if (locationInfo.isSent != 1) {
            locationService.sendLocation();
        }
    }
    //Pull to refresh
    $scope.doRefresh = function () {
        homePageService.gettabs($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.onSwipeRight = function () {
        $('#noteid').fadeOut();
    }

    $scope.saved = function () {
        $('.show_1').css('color', 'rgb(40, 211, 40)');
        $('.show_0').css('color', 'rgb(209, 54, 54)');

    }

    //handles a click on the notification
    $scope.view_notification = function () {
        $location.path("/app/notification");
        $('.notification-number').css('display', 'none');
    }


})

   // <summary>This controller handles the Details of a single coupon</summary>
   // <param name="$scope">Its a variable that is available to both the controller and view</param>
   // <param name="$ionicModal">Provider that handles the POP up screen.</param>
   // <param name="singleCouponService">A service that handles the details coupon data. defined in service.js</param>
   // <param name="$ionicNavBarDelegate">Handles the back button on the view page</param>
   // <param name="$cordovaClipboard">Cordova plugin that handles the copy to clipboard</param>
   // <param name="$cordovaSocialSharing">Cordova plugin that handles the Sharing of coupon</param>
   // <param name="locationService">Sends the geo location to the server</param>
   // <returns></returns>
   // <remarks></remarks>
.controller('CouponCtrl', function ($scope, $stateParams, singleCouponService, $ionicModal, $cordovaClipboard, $cordovaSocialSharing, favoriteService, $window, $cordovaToast) {

    //Fetch the coupon details
    singleCouponService.details($scope, $stateParams.couponId);
    $(document).ready(function ($scope) {
    });

    //Copy the text to clipboard
    $scope.copyToClipBoard = function (text) {
        if (text == "" || text == undefined) {
            $cordovaToast.show("Nothing to copy . .", "long", "center");
        }
        else {
            $cordovaClipboard
                  .copy(text)
                  .then(function () {
                      $cordovaToast.show(text + " copied to clipboard...", "long", "center");
                  }, function () {
                      // error
                      $cordovaToast.show("Copy to clipboard failed...", "long", "center")
                  });
        }
    }

    //Shares the coupon 
    $scope.socialShare = function (tt, url) {
        $cordovaSocialSharing
          .share(tt, null, null, url)
          .then(function (success) {
          }, function (err) {
          });

    }

    //Add the coupon to favourite
    $scope.addFavorite = function (coupon_id, coupon_cs) {
        $scope.coupon_id = coupon_id;

        if (coupon_cs == 0) {
            favoriteService.save($scope);
        }
        else {
            favoriteService.remove($scope);

        }
    }

    //opens the coupon url on another browser
    $scope.func1 = function (coupon_url) {
        $window.open(coupon_url, '_system', 'location=yes'); return false;
    }
})
.controller('addFavoriteCtrl', function ($scope, $stateParams, favoriteService, singleCouponService, $location) {
    singleCouponService.details($scope, $stateParams.singlecouponId);
    favoriteService.save($scope, $stateParams.singlecouponId);
})

   // <summary>This controller handles the Points of indivisual User</summary>
   // <param name="$scope">Its a variable that is available to both the controller and view</param>
   // <param name="$ionicModal">Provider that handles the POP up screen.</param>
   // <param name="pointsService">A service that handles the details Point. defined in service.js</param>
   // <returns></returns>
   // <remarks></remarks>
.controller('PointsCtrl', function ($scope, $stateParams, pointsService, $window) {

    //loads the point data
    pointsService.show($scope);

    //opens the terms on a different window
    $scope.func1 = function () {
        $window.open('http://www.27coupons.com/', '_system', 'location=yes'); return false;
    }
})
// <summary>This controller handles the Stores page</summary>
   // <param name="$scope">Its a variable that is available to both the controller and view</param>
   // <param name="featuredService">A service that fetches the featured stores. defined in service.js</param>
   // <param name="searchService">A service that handles search store. defined in service.js</param>
   // <returns></returns>
   // <remarks></remarks>
.controller('StoresCtrl', function ($scope, featuredService, searchService) {
    $scope.paginationData = {};
    $scope.searchStores = [];

    $scope.load = function () {
    }
    featuredService.feature($scope);
    //searchService.store($scope);
    $scope.doRefresh = function () {
        $scope.paginationData = {};
        $scope.searchStores = [];
        featuredService.feature($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.setCurrent = function (search) {
        $('.placeholder-icon').hide()
        $scope.search_text = search;
        searchService.store($scope);
    }
    $scope.onSwipeRight = function () {
        $('#noteid').fadeOut();
    }

    $scope.view_notification = function () {
        $location.path("/app/notification");
    };
    //implements infinite scrolling
    $scope.loadMoreResults = function () {
        //alert("hi");
        searchService.searchMore($scope);
    };

})
   /*
    <summary>This controller handles the favourite Stores page</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="favouriteStoresService">A service that fetches the favourite stores. defined in service.js</param>
    <param name="$location">angular provider for URL stuff</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('FavstoresCtrl', function ($scope, favouriteStoresService, $location) {

    $scope.paginationData = {};
    $scope.stores = [];
    $scope.page = 1;

    //loads the favourite store
    favouriteStoresService.favstores($scope);


    $('.photo').on((window.ontouchstart ? 'tap' : 'click'), function () {
        $(this).toggleClass('blurry');
    });

    $(function () {
        window.scrollTo(0, 0);
    });
    $scope.onSwipeRight = function () {
        $('#noteid').fadeOut();
    }
    $scope.view_notification = function () {
        $location.path("/app/notification");
    }

    //Implements pull to refresh
    $scope.doRefresh = function () {
        $scope.paginationData = {};
        $scope.stores = [];
        $scope.page = 1;
        // console.log($scope.page);
        favouriteStoresService.favstores($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }

    //implement infinite scrolling
    $scope.loadMore = function () {
        $scope.page++;
        console.log($scope.page);
        favouriteStoresService.loadMoreStores($scope);
        $scope.$broadcast('scroll.infiniteScrollComplete');
    }
})

 /*
    <summary>This controller handles the single Stores page</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="singleStoreService">A service that fetches the details of single store. defined in service.js</param>
    <param name="storeSaveSrevice">A service that saves and unsaves the store. defined in service.js</param>
    <param name="storeSubscribeService">A service that subscribes and unsubscribes stores. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('SinglestoreCtrl', function ($scope, $stateParams, singleStoreService, storeSaveSrevice, storeSubscribeService) {

    //loads the data for a single store.
    singleStoreService.details($scope, $stateParams.storeId);

    //saves or removes a store depending pon the flag
    $scope.store_save = function (store_id, store_fav) {
        $scope.store_id = store_id;
        if (store_fav == 0) {
            storeSaveSrevice.save($scope);
        }
        else {
            storeSaveSrevice.remove($scope);

        }
    }
    //subscribes or unsubscribes a store depending pon the flag
    $scope.store_subscribe = function (store_id, store_alert) {
        $scope.store_id = store_id;
        if (store_alert == 0) {
            storeSubscribeService.subscribe($scope);
        }
        else {
            storeSubscribeService.unsubscribe($scope);
        }
    }
    //handles the refresh
    $scope.doRefresh = function () {
        singleStoreService.details($scope, $stateParams.storeId);
        $scope.$broadcast('scroll.refreshComplete');
    };
})

/*
    <summary>This controller handles the category all page</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="categoriesListAllService">A service that fetches the categories. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('CategorieslistallCtrl', function ($scope, categoriesListAllService, $location) {

    //loads the category
    categoriesListAllService.categorylist($scope);

    //handles the refresh
    $scope.doRefresh = function () {
        categoriesListAllService.categorylist($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }
    $scope.onSwipeRight = function () {
        $('#noteid').fadeOut();
    }
    $scope.view_notification = function () {
        $location.path("/app/notification");
    }
})


/*
    <summary>This controller handles the Notification</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="notificationService">A service that handles the list of notification. defined in service.js</param>
    <param name="categoriesListAllService">A service that handles a particular of notification. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('NotificationCtrl', function ($scope, notificationService, notificationDetailService) {

    //loads the notification
    notificationService.show($scope);


    //handles the pull to refresh
    $scope.doRefresh = function () {
        notificationService.show($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }

    //handles the detail of a notification
    $scope.notification_detail = function (api_node, note_id) {
        $scope.api_node = api_node;
        $scope.id = note_id;
        var str = JSON.stringify(api_node);
        var res = str.split("/");
        $scope.category = res[2];
        $scope.fourcharacter = res[3];
        notificationDetailService.select($scope);
    }

    //delete a notification
    $scope.deleteNotification = function () {
        notificationService.delete($scope);
    }
})

    /*
    <summary>This controller handles the Single Category</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="categoriesListsingleService">A service that handles a particular category. defined in service.js</param>
    <param name="categorySaveService">A service that handles save and unsave of category. defined in service.js</param>
    <param name="categorySubscribeService">A service that handles subscribe and unsubscribe of category. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('CategorieslistsingleCtrl', function ($scope, $stateParams, categoriesListsingleService, categorySaveService, categorySubscribeService) {
    $scope.paginationData = {};
    $scope.tabs = [];
    $scope.newTabs = [];
    $scope.category = [];
    $scope.totalRecords;
    
    //loads the data for a particular category
    categoriesListsingleService.categorydetails($scope, $stateParams.categoryId);

    //handles the refresh
    $scope.doRefresh = function () {

        $scope.paginationData = {};
        $scope.tabs = [];
        $scope.newTabs = [];
        $scope.category = [];
        $scope.totalRecords;

        categoriesListsingleService.categorydetails($scope, $stateParams.categoryId);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    };


    //handles the pagination
    $scope.loadMoreCategories = function () {
        categoriesListsingleService.loadMoreCategories($scope)
    }

    //saves a category
    $scope.cat_save = function (category_id, category_fav) {

        if (category_fav == 0) {
            $('.error').removeClass('ng-hide');
            categorySaveService.save($scope);
        }
        else {
            $('.error1').removeClass('ng-hide');
            categorySaveService.remove($scope);
        }
    }

    //subscribes a category
    $scope.cat_subscribe = function (category_id, category_alert) {
        if (category_alert == 0) {
            categorySubscribeService.subscribe($scope);
        }
        else {
            categorySubscribeService.unsubscribe($scope);
        }
    }
})

    /*
    <summary>This controller handles list of banks</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="bankService">A service that handles data related to bank. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('BankCtrl', function ($scope, bankService, $ionicPopup, $location) {

    $scope.paginationData = {};
    $scope.banks = [];
    $scope.page = 1;

    //loads banks
    bankService.banklist($scope);

    //handles refresh
    $scope.doRefresh = function () {

        $scope.paginationData = {};
        $scope.banks = [];
        $scope.page = 1;
        bankService.banklist($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }
    var $x = bankService.banklist($scope);

    $scope.onSwipeRight = function () {
        $('#noteid').fadeOut();
    }
    $scope.view_notification = function () {
        $location.path("/app/notification");
    }
    

    //handles pagination 
    $scope.loadMore = function ($scope) {
        $scope.page++;
        bankService.loadMoreBanks($scope);
        $scope.$broadcast('scroll.infiniteScrollComplete');
    };

})

     /*
    <summary>This controller handles list of banks</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="bankDetailsService">A service that handles data related to a particular bank. defined in service.js</param>
    <param name="bankSaveService">A service that handles save and unsave of bank. defined in service.js</param>
    <param name="bankSubscribeService">A service that handles subscribe and unsubscribe of bank. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('BanksingleCtrl', function ($scope, $stateParams, bankDetailsService, bankSaveService, bankSubscribeService) {
    $scope.paginationData = {};
    $scope.bank = [];
    $scope.page = 1;
    $scope.coupons = [];
    bankDetailsService.banksingledetails($scope, $stateParams.bankId);
    $scope.doRefresh = function () {
        $scope.paginationData = {};
        $scope.bank = [];
        $scope.page = 1;
        $scope.coupons = [];
        bankDetailsService.banksingledetails($scope, $stateParams.bankId);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }

    //saves or unsave a bank
    $scope.bank_save = function (bank_id, bank_fav) {
        $scope.bank_id = bank_id;
        if (bank_fav == 0) {
            bankSaveService.save($scope);
        }
        else {
            bankSaveService.remove($scope);
        }
    }

    //subscribe or unsubscribe of bank
    $scope.bank_subscribe = function (bank_id, bank_alert) {
        $scope.bank_id = bank_id;
        if (bank_alert == 0) {
            bankSubscribeService.subscribe($scope);
        }
        else {
            bankSubscribeService.unsubscribe($scope);
        }
    }

    //handles pagination
    $scope.loadMore = function () {
        $scope.page++;
        bankDetailsService.loadMoreBanks($scope);
        $scope.$broadcast('scroll.infiniteScrollComplete');

    };
})
/*
    <summary>This controller handles Alert Page</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="bankDetailsService">A service that handles data related to a particular bank. defined in service.js</param>
    <param name="bankSaveService">A service that handles save and unsave of bank. defined in service.js</param>
    <param name="bankSubscribeService">A service that handles subscribe and unsubscribe of bank. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('AlertCtrl', function ($scope, alertService) {
    alertService.listsingle($scope);
    $scope.doRefresh = function () {
        alertService.listsingle($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    }
})

    /*
    <summary>This controller handles the functionalities of indivisual Alerts</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="singleAlertService">A service that the datails of a single alert. defined in service.js</param>
    <param name="deleteAlertService">A service that handles deleting a single alert. defined in service.js</param>
    <param name="activateAlertService">A service that handles activaing or deactivating a single alert. defined in service.js</param>
    <param name="pauseAlertService">A service that handles pauses or resumes a single alert. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('AlertsingleCtrl', function ($scope, $stateParams, singleAlertService, deleteAlertService, activateAlertService, pauseAlertService) {

    $scope.paginationData = {};
    $scope.tabs = {};
    $scope.alert = {};
    $scope.totalRecords;
    //$scope.coupon = [];

    //list the details of a single Alert
    singleAlertService.listsingle($scope, $stateParams.alertId);

    //deletes an alert
    $scope.deleteAlert = function (alert_id) {
        $scope.alert_id = alert_id;
        deleteAlertService.del($scope);
    }
    //activates an alert
    $scope.activateAlert = function (alert_id) {
        $scope.alert_id = alert_id;
        activateAlertService.activate($scope);
    }
    //Pauses an alert
    $scope.pauseAlert = function (alert_id) {
        $scope.alert_id = alert_id;
        pauseAlertService.pause($scope);
    }
    //handles pull to refresh
    $scope.doRefresh = function () {
        $scope.paginationData = {};
        $scope.tabs = {};
        $scope.alert = {};
        $scope.totalRecords;
        singleAlertService.listsingle($scope, $stateParams.alertId);
        $scope.$broadcast('scroll.refreshComplete');

    };
    //handles pagination
    $scope.loadMoreResults = function () {
        // alert("hello");
        singleAlertService.loadMoreAlerts($scope);
    }
})
     /*
    <summary>This controller handles the functionalities of Refer and Earn System</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="referService">A service that the datails of Refer and Earn Page. defined in service.js</param>
    <param name="$cordovaSocialSharing">Codova plugin for native sharing</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('ReferEarnCtrl', function ($scope, $cordovaSocialSharing, referService, $window) {
    //Handles the refer page
    referService.earn($scope);

    //Shares Via Email
    $scope.shareViaEmail = function () {
        var message = $scope.messageText + $scope.linkToBeSharedViaEmail;
        $cordovaSocialSharing
              .shareViaEmail(message, $scope.messageSubject, null, null, null)
              .then(function (success) {
              }, function (err) {
              });
    }

    //Shares via social network
    $scope.shareViaSocial = function () {
        var message = $scope.messageText + $scope.linkToBeSharedViaEmail;

        $cordovaSocialSharing
              .share(message, $scope.messageSubject, null, $scope.linkToBeShared)
              .then(function (success) {
              }, function (err) {
              });
    }
    $scope.func1 = function () {
        $window.open('http://www.27coupons.com/', '_system', 'location=yes'); return false;
    }

})

    /*
    <summary>This controller handles the functionalities of Refer and Earn System</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="couponService">A service that Handles the saves coupon page. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('SavedCouponCtrl', function ($scope, $stateParams, couponService, $location) {

    $scope.paginationData = {};
    $scope.tabs = [];
    $scope.newTabs = [];
    $scope.totalRecords;
    //loads the save coupon
    couponService.saved($scope);

    //Handles pull to refresh
    $scope.doRefresh = function () {

        $scope.paginationData = {};
        $scope.tabs = [];
        $scope.newTabs = [];
        $scope.totalRecords = 0;
        couponService.saved($scope);
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
    };
    $scope.onSwipeRight = function () {
        $('#noteid').fadeOut();
    };
    $scope.view_notification = function () {
        $location.path("/app/notification");
    };
    //handles infinite scrolling
    $scope.loadMoreResults = function () {
        couponService.loadMoreResults($scope);
    }

})
     /*
    <summary>This controller handles the functionalities of Refer and Earn System</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="createAlertService">A service that Creates alerts. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('CreateAlertCtrl', function ($scope, createAlertService, storess) {
    createAlertService.create($scope);
    $scope.setCurrent = function (search) {
        $scope.search = search
        var x = search.length;
        if (x > 1) {
            storess.stor($scope);
        }
    }

    $scope.scheduleList = [
                        { text: "Monday", checked: false, id: 1 },
                        { text: "Tuesday", checked: false, id: 2 },
                        { text: "Wednesday", checked: false, id: 3 },
                        { text: "Thursday", checked: false, id: 4 },
                        { text: "Friday", checked: false, id: 5 },
                        { text: "Saturday", checked: false, id: 6 },
                        { text: "Sunday", checked: false, id: 7 }
    ];

    $scope.form = {};
    $scope.form.stores = [];
    $scope.form.categories = [];
    $scope.form.Schedule = [];
    $scope.form.coupon = 0;
    id = 0;

    $scope.load = function () {
    };
    $scope.value = function () {
        $scope.form.coupon = 1;
        $('.color_ch').addClass('button-fade-gray');
        $('.color_ch1').removeClass('button-fade-gray');
        $('.color_ch1').addClass('button-gray');
        $('.color_ch').removeClass('button-gray');
    }
    $scope.any_value = function () {
        $scope.form.coupon = 0;
        $('.color_ch1').addClass('button-fade-gray');
        $('.color_ch').removeClass('button-fade-gray');
        $('.color_ch').addClass('button-gray');
        $('.color_ch1').removeClass('button-gray');
    }
})
    /*
    <summary>This controller saves an alert</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="saveAlertService">A service that Creates alerts. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('SaveAlertCtrl', function ($scope, saveAlertService) {

    // Form data for the alert creation
    $scope.createData = {};
    $scope.form.store_id = [];
    $scope.form.categories_id = [];
    $scope.form.schedule_id = [];

    // Perform the create action when the user clicks on save button.
    $scope.createAlert = function () {
        angular.forEach($scope.form.stores, function (value, index) {
            $scope.form.store_id.push(value.id);
        })
        angular.forEach($scope.form.categories, function (value, index) {
            $scope.form.categories_id.push(value.id);
        })
        angular.forEach($scope.scheduleList, function (value, index) {
            if (value.checked == true) {
                $scope.form.schedule_id.push(value.id);
            };
        })
        saveAlertService.create($scope.form, $scope);
    };
})
    /*
    <summary>This controller saves an alert</summary>
    <param name="$scope">Its a variable that is available to both the controller and view</param>
    <param name="profileService">fetches the profile data. defined in service.js</param>
    <returns></returns>
    <remarks></remarks>
   */
.controller('ProfileCtrl', function ($scope, profileService, $window) {
    profileService.getProfile($scope);
    $scope.openLink = function () {
        //alert("hi")
        $window.open("http://www.27coupons.com/account/profile/", '_system', 'location=yes'); return false;
    }
});